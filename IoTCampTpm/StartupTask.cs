﻿using GrovePi;
using GrovePi.I2CDevices;
using GrovePi.Sensors;
using Microsoft.Azure.Devices.Client;
using Microsoft.Azure.Devices.Shared;
using Microsoft.Devices.Tpm;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.System.Threading;

namespace IoTCampTpm
{
    public sealed class StartupTask : IBackgroundTask
    {
        BackgroundTaskDeferral deferral;
        ThreadPoolTimer timer;

        DeviceClient deviceClient;
        int sendFrequency = 5;

        /**********************************************
            Hardware devices
        ***********************************************/
        private IRgbLcdDisplay display;
        private IRotaryAngleSensor angleSensor;
        private ITemperatureAndHumiditySensor temperatureSensor;

        string deviceId;

        /**********************************************
            Placeholder: Add a Tpm Device object
        ***********************************************/
        TpmDevice tpmDevice;


        /**********************************************
            Placeholder: Add a Twin management object
        ***********************************************/
        TwinCollection reportedProperties = new TwinCollection();

        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            deferral = taskInstance.GetDeferral();

            InitializeHardware();

            /**********************************************
                Placeholder: Using the TPM chip in code
            ***********************************************/
            tpmDevice = new TpmDevice(0);

            deviceId = tpmDevice.GetDeviceId();
            deviceClient = DeviceClient.CreateFromConnectionString(tpmDevice.GetConnectionString(), TransportType.Mqtt);


            /**********************************************
                Placeholder: Register a Direct Method
            ***********************************************/
            await deviceClient.SetMethodHandlerAsync("SendDiagnostics", SendDiagnostics, null);


            /**********************************************
                Placeholder: Initialize device twin properties
            ***********************************************/
            await InitTelemetry();

            await deviceClient.SetDesiredPropertyUpdateCallbackAsync(OnDesiredPropertyChanged, null);

            timer = ThreadPoolTimer.CreatePeriodicTimer(Timer_Tick, TimeSpan.FromSeconds(sendFrequency));
        }


        private void InitializeHardware()
        {
            display = DeviceFactory.Build.RgbLcdDisplay();
            angleSensor = DeviceFactory.Build.RotaryAngleSensor(Pin.AnalogPin2);
            temperatureSensor = DeviceFactory.Build.TemperatureAndHumiditySensor(Pin.AnalogPin1, Model.Dht11);
        }


        /**********************************************
            Placeholder: Timer_Tick Code
        ***********************************************/
        private async void Timer_Tick(ThreadPoolTimer timer)
        {
            byte rgbVal;
            double currentTemp;

            rgbVal = Convert.ToByte(angleSensor.SensorValue() / 4);

            currentTemp = temperatureSensor.TemperatureInCelsius();
            currentTemp = ConvertTemp.ConvertCelsiusToFahrenheit(currentTemp);

            var now = DateTime.Now.ToString("H:mm:ss");
            display.SetText($"Temp: {currentTemp.ToString("F")}      Now:  {now}")
                .SetBacklightRgb(124, rgbVal, 65);

            //Send telemetry to the cloud
            await SendDeviceToCloudMessagesAsync(currentTemp, humidity: 32);
            await ReceiveCloudToDeviceAsync();
        }


        /**********************************************
            Placeholder: SendDeviceToCloudMessageAsnyc
        ***********************************************/
        private async Task SendDeviceToCloudMessagesAsync(double temperature, double humidity)
        {
            var telemetryDataPoint = new
            {
                deviceId,
                temperature = Math.Round(temperature, 2), //Changed this from .ToString("F") so that we have numeric data for PowerBI
                humidity
            };

            string messageString = JsonConvert.SerializeObject(telemetryDataPoint);
            using (var message = new Message(Encoding.ASCII.GetBytes(messageString)))
            {
                await deviceClient.SendEventAsync(message);
            }

            Debug.WriteLine($"{DateTime.Now} -> Sent message: {messageString}");
        }

        /**********************************************
            Placeholder: Receive cloud to device message
        ***********************************************/
        private async Task ReceiveCloudToDeviceAsync()
        {
            try
            {
                var receivedMessage = await deviceClient.ReceiveAsync();
                if (receivedMessage != null)
                {
                    Debug.WriteLine($"{DateTime.Now} -> Received message: {Encoding.ASCII.GetString(receivedMessage.GetBytes())}");
                    
                    await deviceClient.CompleteAsync(receivedMessage);
                    receivedMessage.Dispose();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }

        }

        /**********************************************
            Placeholder: Direct Method callback
        ***********************************************/
        private Task<MethodResponse> SendDiagnostics(MethodRequest methodRequest, object userContext)
        {
            Debug.WriteLine($"Method payload -> {methodRequest.DataAsJson}");
            Debug.WriteLine($"Returning response for method {methodRequest.Name}");

            return Task.FromResult(new MethodResponse(Encoding.UTF8.GetBytes("{\"Status\":\"Level 1 diagnostic complete!\"}"), 200));
        }


        /**********************************************
            Placeholder: Report initial telemetry settings
        ***********************************************/
        private async Task InitTelemetry()
        {
            Debug.WriteLine("Report initial telemetry config:");

            TwinCollection telemetryConfig = new TwinCollection
            {
                ["configId"] = "0",
                ["sendFrequency"] = "5"
            };
            reportedProperties["telemetryConfig"] = telemetryConfig;

            Debug.WriteLine(reportedProperties.ToJson(Formatting.Indented));

            await deviceClient.UpdateReportedPropertiesAsync(reportedProperties);
        }


        /**********************************************
            Placeholder: Callback for Desired Propert changes
        ***********************************************/
        private async Task OnDesiredPropertyChanged(TwinCollection desiredProperties, object userContext)
        {
            Debug.WriteLine($"Desired property change:\n {desiredProperties.ToJson(Formatting.Indented)}");

            var currentTelemetryConfig = reportedProperties["telemetryConfig"];
            var desiredTelemetryConfig = desiredProperties["telemetryConfig"];

            if ((desiredTelemetryConfig != null) && (desiredTelemetryConfig["configId"] != currentTelemetryConfig["configId"]))
            {
                Debug.WriteLine("Initiating config change");

                currentTelemetryConfig["status"] = "Pending";
                currentTelemetryConfig["pendingConfig"] = desiredTelemetryConfig;

                await deviceClient.UpdateReportedPropertiesAsync(reportedProperties);

                await CompleteConfigChange();
            }
        }

        /**********************************************
            Placeholder: Updates the actual properties
        ***********************************************/
        private async Task CompleteConfigChange()
        {
            var currentTelemetryConfig = reportedProperties["telemetryConfig"];

            Debug.WriteLine("Completing config change");

            currentTelemetryConfig["configId"] = currentTelemetryConfig["pendingConfig"]["configId"];
            currentTelemetryConfig["sendFrequency"] = currentTelemetryConfig["pendingConfig"]["sendFrequency"];
            currentTelemetryConfig["status"] = "Success";
            currentTelemetryConfig["pendingConfig"] = null;

            //Cancel and reset out perdioc timer
            sendFrequency = currentTelemetryConfig["sendFrequency"];
            timer.Cancel();
            timer = ThreadPoolTimer.CreatePeriodicTimer(Timer_Tick, TimeSpan.FromSeconds(sendFrequency));

            await deviceClient.UpdateReportedPropertiesAsync(reportedProperties);
        }

    }
}
